# harulisp

A modern lisp dialect.

# Purpose

- harulisp is NOT for enbedded devices, such as `raspberry pi`.
- harulisp is NOT for web apps, so harulisp cannot use as an alternative JavaScript.
- harulisp is JUST for command-line-apps.
- Functional Programming
- Strong type checking
- Easier to learn
- Easier to write
- Unit test integration.
- Module system integration.
- A package manager integration for harulisp.

## Naming

First, `HARUki's LISP.`
Next, `HARULISP`
Finally, `harulisp`
