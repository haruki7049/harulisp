# CONTRIBUTING GUIDE

This Guide is written in Japanese for Japanese Developer. このガイドは日本人の開発者用に日本語で書かれています。

## Issueの作成

- Issueを立てる前には必ず同様のIssueが存在しない事を確認してください。もしもIssueの内容が被っていた場合には`Not Planned`としてIssueを閉じてください。
- Issueをグループにしたい場合はマイルストーンを作成する事を検討してください。

### Issueの命名規則

- Issueの命名規則は[Conventional Commits:v1.0.0](https://www.conventionalcommits.org/ja/v1.0.0/)に従ってください。

以下に例を提示します。

`feat: Adding regex_lite in Cargo.toml`, `fix: Fixing typo`, `refactor: Deleting hoge function`, `docs: Writing CONTRIBUTING.md`

## ブランチの命名規則

ブランチの命名は以下の様に行なってください。

```txt
(Issueの型)/(Issueの説明文を全て小文字で表現、程良く短くしてハイフン区切りにした文)
```

以下に例を提示します。

`feat/adding-regex_lite`, `fix/fixing-typo`, `refactor/deleting-hoge-function`, `docs/writing-contributing`

## MergeRequestの作成

MergeRequestは必ずIssueに関連づけて作成してください。

### MergeRequestの命名規則

関連づけたIssueの名前と一致させてください。

## コードの修正

以下のフローに従ってください。

1. Issueを作成する。
2. Issueに対応させたMergeRequestを作成する。
3. MergeRequestにレビューを貰い、マージ。

## Review

レビューを貰いマージが出来るようになり、作業を終えた後には作業ブランチを削除してください。
