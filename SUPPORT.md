# SUPPORT

This Document is in Japanese. English version is none. この文書は日本語版です。英語版はありません。

harulisp(以降当該ソフトウェア)は[MITライセンス](../LICENSE) (以降当該ライセンス)の下で提供されています。当該ソフトウェアは当該ライセンスを理解、同意した人のみが使用できます。私たちは当該ソフトウェアに起因する事件、事象に一切責任を負いません。
